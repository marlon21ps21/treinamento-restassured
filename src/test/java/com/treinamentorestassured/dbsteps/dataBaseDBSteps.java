package com.treinamentorestassured.dbsteps;

import com.treinamentorestassured.utils.DBUtils;
import com.treinamentorestassured.utils.GeneralUtils;

public class dataBaseDBSteps {
	
	private static String queriesPath = "src/test/java/com/treinamentorestassured/queries/";
	
	public static void createDataBase() {
		String query = GeneralUtils.readFileToAString(queriesPath+"createDataBase.sql");
		DBUtils.executeCreateDataBaseQuery(query);
	}
	
	public static void createTableProdutos() {
		String query = GeneralUtils.readFileToAString(queriesPath+"createTableProdutos.sql");
		DBUtils.executeUpdateQuery(query);
	}
	
	public static void dropDataBase() {
		String query = GeneralUtils.readFileToAString(queriesPath+"dropDataBase.sql");
		DBUtils.executeUpdateQuery(query);
	}

}
