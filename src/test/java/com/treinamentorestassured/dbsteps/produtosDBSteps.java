package com.treinamentorestassured.dbsteps;

import java.util.List;

import com.treinamentorestassured.forms.ProdutoForm;
import com.treinamentorestassured.utils.DBUtils;
import com.treinamentorestassured.utils.GeneralUtils;

public class produtosDBSteps {

	private static String queriesPath = "src/test/java/com/treinamentorestassured/queries/";

	public static  List<ProdutoForm> retornaProdutos() {
		String query = GeneralUtils.readFileToAString(queriesPath + "retornarProdutosQuery.sql");
		return DBUtils.getProdutosQuery(query);
	}
	
	public static void cadastraProdutos() {
		String query = GeneralUtils.readFileToAString(queriesPath+"cadastrarProdutosQuery.sql");
		DBUtils.executeUpdateQuery(query);
	}
	
	public static void excluirProdutos() {
		String query = GeneralUtils.readFileToAString(queriesPath+"excluirProdutosQuery.sql");
		DBUtils.executeUpdateQuery(query);
	}
	
}
