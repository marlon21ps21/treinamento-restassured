package com.treinamentorestassured.tests.produtos;

import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.forms.ProdutoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;
import com.treinamentorestassured.requests.produtos.DeleteProdutosRequest;
import com.treinamentorestassured.requests.produtos.PostProdutosRequest;
import com.treinamentorestassured.requests.produtos.PutProdutosRequest;

import io.restassured.response.Response;

public class PutProdutosTest extends TestBase {

	PostProdutosRequest postProdutosRequest;
	PutProdutosRequest putProdutosRequest;
	DeleteProdutosRequest deleteProdutosRequest;

	@Test
	public void DadoFormularioDeProdutoValidoDeveAlterarComSucesso() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		ProdutoForm produto = new ProdutoForm();
		produto.setNome("Caixa de Caneta");
		produto.setPreco("25");
		produto.setDescricao("Canetas Boas");
		produto.setQuantidade("3");
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		String mensagemEsperada = "Registro alterado com sucesso";
		int statusCodeEsperado = HttpStatus.SC_OK;
		String token;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();
		token = response.body().jsonPath().get("authorization").toString();

		GlobalParameters.TOKEN = token;

		postProdutosRequest = new PostProdutosRequest();
		postProdutosRequest.setProduto(produto);
		Response postProdutoResponse = postProdutosRequest.executeRequest();
		String produtoId = postProdutoResponse.body().jsonPath().get("_id").toString();

		putProdutosRequest = new PutProdutosRequest(produtoId);
		putProdutosRequest.setProduto(produto);
		Response putProdutoResponse = putProdutosRequest.executeRequest();
		String message = putProdutoResponse.body().jsonPath().get("message").toString();
		int putStatusCode = putProdutoResponse.getStatusCode();

		deleteProdutosRequest = new DeleteProdutosRequest(produtoId);
		deleteProdutosRequest.executeRequest();

		assertEquals(putStatusCode, statusCodeEsperado);
		assertEquals(message, mensagemEsperada);

	}

}
