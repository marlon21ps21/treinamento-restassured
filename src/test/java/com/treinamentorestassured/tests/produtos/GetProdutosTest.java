package com.treinamentorestassured.tests.produtos;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.requests.produtos.GetProdutosRequest;

import io.restassured.response.Response;

public class GetProdutosTest extends TestBase {

	GetProdutosRequest getProdutosRequest;

	@Test
	public void DeveBuscarListaDeProdutosSemFiltro() {

		SoftAssert softAssert = new SoftAssert();
		
		int statusCodeEsperado = HttpStatus.SC_OK;
		String valorEsperado = "2";
		
		getProdutosRequest = new GetProdutosRequest();
		Response getProdutoResponse = getProdutosRequest.executeRequest();

		softAssert.assertEquals(getProdutoResponse.getStatusCode(), statusCodeEsperado);
		softAssert.assertEquals(getProdutoResponse.body().jsonPath().get("quantidade").toString(), valorEsperado);
		softAssert.assertAll();

	}

	@Test
	public void deveBuscarProdutoPeloIdQuery() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String query = "_id";
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(query, idProduto);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0]._id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoPeloNomeQuery() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String query = "nome";
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(query, nome);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0]._id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoPeloPrecoQuery() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String query = "preco";
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(query, preco);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0]._id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoPeloDescricaoQuery() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String query = "descricao";
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(query, descricao);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0]._id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoPeloQuantidadeQuery() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String query = "quantidade";
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(query, quantidade);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0]._id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("produtos[0].quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoPeloIdPath() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_OK;
		String idProduto = "K6leHdftCeOJj8BJ";
		String nome = "Samsung 60 polegadas";
		String preco = "5240";
		String descricao = "TV";
		String quantidade = "49977";

		getProdutosRequest = new GetProdutosRequest(idProduto);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("_id").toString(), idProduto);
		softAssert.assertEquals(response.body().jsonPath().get("nome").toString(), nome);
		softAssert.assertEquals(response.body().jsonPath().get("preco").toString(), preco);
		softAssert.assertEquals(response.body().jsonPath().get("descricao").toString(), descricao);
		softAssert.assertEquals(response.body().jsonPath().get("quantidade").toString(), quantidade);
		softAssert.assertAll();

	}
	
	@Test
	public void deveBuscarProdutoQueNaoExistePeloIdPath() {
		SoftAssert softAssert = new SoftAssert();

		int statusCodeEspeado = HttpStatus.SC_BAD_REQUEST;
		String idProduto = "AAAAAAAAAAAAAAAA";
		String message = "Produto não encontrado";

		getProdutosRequest = new GetProdutosRequest(idProduto);
		Response response = getProdutosRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(), statusCodeEspeado);
		softAssert.assertEquals(response.body().jsonPath().get("message").toString(), message);
		softAssert.assertAll();

	}

}
