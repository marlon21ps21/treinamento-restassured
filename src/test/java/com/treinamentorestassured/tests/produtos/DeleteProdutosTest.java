package com.treinamentorestassured.tests.produtos;

import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.forms.ProdutoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;
import com.treinamentorestassured.requests.produtos.DeleteProdutosRequest;
import com.treinamentorestassured.requests.produtos.PostProdutosRequest;

import io.restassured.response.Response;

public class DeleteProdutosTest extends TestBase {
	
	DeleteProdutosRequest deleteProdutosRequest;
	PostProdutosRequest postProdutosRequest;

	@Test
	public void DadoProdutoCadastradoQuandoTentaRemoverEntaoDeveExibirMensagemDeSucesso() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		ProdutoForm produto = new ProdutoForm();
		produto.setNome("Celular J7");
		produto.setPreco("800");
		produto.setDescricao("Celular Bom");
		produto.setQuantidade("5");
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		String mensagemEsperada = "Registro excluído com sucesso";
		int statusCodeEsperado = HttpStatus.SC_OK;
		String token;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response autenticationResponse = autenticacaoRequest.executeRequest();
		token = autenticationResponse.body().jsonPath().get("authorization").toString();
		
		GlobalParameters.TOKEN = token;
		postProdutosRequest = new PostProdutosRequest();
		postProdutosRequest.setProduto(produto);
		Response postProdutoResponse = postProdutosRequest.executeRequest();
		String produtoId = postProdutoResponse.body().jsonPath().get("_id").toString();
		
		deleteProdutosRequest = new DeleteProdutosRequest(produtoId);
		Response deleteProdutoResponse = deleteProdutosRequest.executeRequest();
		String message = deleteProdutoResponse.body().jsonPath().get("message").toString();
		int deleteStatusCode = deleteProdutoResponse.getStatusCode();

		assertEquals(deleteStatusCode, statusCodeEsperado);
		assertEquals(message, mensagemEsperada);
	}

}
