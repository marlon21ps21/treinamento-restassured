package com.treinamentorestassured.tests.contratos;

import static org.testng.Assert.assertEquals;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.requests.carrinhos.GetCarrinhosRequest;
import com.treinamentorestassured.requests.produtos.GetProdutosRequest;
import com.treinamentorestassured.requests.usuarios.GetUsuarioRequest;
import com.treinamentorestassured.steps.carrinhos.ListarCarrinhosSteps;
import com.treinamentorestassured.steps.produtos.ListarProdutosSteps;

import io.restassured.response.Response;

public class AllGetContratoTest extends TestBase {

	GetUsuarioRequest getUsuarioRequest;
	GetCarrinhosRequest getCarrinhosRequest;
	private GetProdutosRequest getProdutosRequest;

	@Test
	public void QuandoBuscarUsuariosDeveValidarContrato() {
		int statusCodeEsperado = HttpStatus.SC_OK;

		getUsuarioRequest = new GetUsuarioRequest();
		Response getUsuarioResponse = getUsuarioRequest.executeRequest();

		assertEquals(getUsuarioResponse.getStatusCode(), statusCodeEsperado);
		getUsuarioResponse.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetUsuarioContrato.json"));

	}

	@Test
	public void QuandoBuscarProdutoDeveValidarContrato() {
		int statusCodeEsperado = HttpStatus.SC_OK;

		getProdutosRequest = new GetProdutosRequest();
		Response getProdutoResponse = getProdutosRequest.executeRequest();

		assertEquals(getProdutoResponse.getStatusCode(), statusCodeEsperado);
		getProdutoResponse.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetProdutoContrato.json"));
	}

	@Test
	public void QuandoBuscarCarrinhoDeveValidarContrato() {
		int statusCodeEsperado = HttpStatus.SC_OK;

		getCarrinhosRequest = new GetCarrinhosRequest();
		Response getCarrinhosResponse = getCarrinhosRequest.executeRequest();
		int statusCode = getCarrinhosResponse.getStatusCode();
		assertEquals(statusCode, statusCodeEsperado);
		getCarrinhosResponse.then().assertThat()
				.body(matchesJsonSchemaInClasspath("contratos/GetCarrinhosContrato.json"));
	}

}
