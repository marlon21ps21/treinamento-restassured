package com.treinamentorestassured.tests.contratos;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;

import io.restassured.response.Response;

public class AutenticacaoContratoTest extends TestBase{
	
	@Test
	public void DadoUmUsuarioValidoDeveValidarContratoDeLogin() {
		
		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		int statusCodeEsperado = HttpStatus.SC_OK;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/LoginContrato.json"));
		assertEquals(response.getStatusCode(),statusCodeEsperado );
		
	}
	
}
