package com.treinamentorestassured.tests.dbprodutos;

import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.dbsteps.produtosDBSteps;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.forms.ProdutoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;
import com.treinamentorestassured.requests.produtos.DeleteProdutosRequest;
import com.treinamentorestassured.requests.produtos.PostProdutosRequest;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class PostProdutosDBTest extends TestBase {
	
	private PostProdutosRequest postProdutosRequest;
	private DeleteProdutosRequest deleteProdutosRequest;

//	@BeforeMethod
//	public void beforSuiteDB() {
//		produtosDBSteps.cadastraProdutos();
//	}
//	
//	@AfterMethod
//    public void afterSuiteDB(){
//        produtosDBSteps.excluirProdutos();
//    }

	@Test
	public void DadoMassaDeDadosInseridaNoBancoDeveCadastrarProdutosComSucesso() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
//		ProdutoForm produto = produtosDBSteps.retornaProdutos().
//				get(GeneralUtils.getRandomNumber(19));
		ProdutoForm produto = new ProdutoForm();
		produto.setNome("Tablet");
		produto.setPreco("1400");
		produto.setDescricao("Tablet bom");
		produto.setQuantidade("2");
		
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		
		String mensagemEsperada = "Cadastro realizado com sucesso";
		int statusCodeEsperado = HttpStatus.SC_CREATED;
		String token;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();
		token = response.body().jsonPath().get("authorization").toString();
		
		GlobalParameters.TOKEN = token;
		postProdutosRequest = new PostProdutosRequest();
		postProdutosRequest.setProduto(produto);
		Response postProdutoResponse = postProdutosRequest.executeRequest();
		String produtoId = postProdutoResponse.body().jsonPath().get("_id").toString();
		String message = postProdutoResponse.body().jsonPath().get("message").toString();
		int postStatusCode = postProdutoResponse.getStatusCode();
		
		deleteProdutosRequest = new DeleteProdutosRequest(produtoId);
		deleteProdutosRequest.executeRequest();
	
		assertEquals(postStatusCode,statusCodeEsperado);
		assertEquals(message, mensagemEsperada);
	}
}
