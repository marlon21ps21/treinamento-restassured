package com.treinamentorestassured.tests.carrinhos;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.dbsteps.produtosDBSteps;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.forms.CarrinhosForm;
import com.treinamentorestassured.forms.CarrinhosProdutosForm;
import com.treinamentorestassured.forms.ProdutoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;
import com.treinamentorestassured.requests.carrinhos.CancelarCarrinhosRequest;
import com.treinamentorestassured.requests.carrinhos.PostCarrinhosRequest;
import com.treinamentorestassured.requests.produtos.DeleteProdutosRequest;
import com.treinamentorestassured.requests.produtos.PostProdutosRequest;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class PostCarrinhosTest extends TestBase {

	private PostProdutosRequest postProdutosRequest1;
	private PostProdutosRequest postProdutosRequest2;
	private PostCarrinhosRequest postCarrinhosRequest;
	private CancelarCarrinhosRequest cancelarCarrinhosRequest;
	private DeleteProdutosRequest deleteProdutosRequest;
	private DeleteProdutosRequest deleteProdutosRequest2;

	@BeforeMethod
	public void beforSuiteDB() {
		produtosDBSteps.cadastraProdutos();
	}

	@AfterMethod
	public void afterSuiteDB() {
		produtosDBSteps.excluirProdutos();
	}

	@Test
	public void DadoProdutosValidosDeveAdicinalosUmCarrinho() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		ProdutoForm produto1 = produtosDBSteps.retornaProdutos().get(GeneralUtils.getRandomNumber(19));
		ProdutoForm produto2 = produtosDBSteps.retornaProdutos().get(GeneralUtils.getRandomNumber(19));
		CarrinhosProdutosForm carrinhoProduto1;
		CarrinhosProdutosForm carrinhoProduto2;
		List<CarrinhosProdutosForm> produtos = new ArrayList<>();
		CarrinhosForm carrinho = new CarrinhosForm();
		int statusCodeEsperado = HttpStatus.SC_CREATED;
		String token;
		
		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();
		token = response.body().jsonPath().get("authorization").toString();
		
		GlobalParameters.TOKEN = token;

		postProdutosRequest1 = new PostProdutosRequest();
		postProdutosRequest1.setProduto(produto1);
		Response postProdutoResponse1 = postProdutosRequest1.executeRequest();
		String produtoId1 = postProdutoResponse1.body().jsonPath().get("_id").toString();

		postProdutosRequest2 = new PostProdutosRequest();
		postProdutosRequest2.setProduto(produto2);
		Response postProdutoResponse2 = postProdutosRequest2.executeRequest();
		String produtoId2 = postProdutoResponse2.body().jsonPath().get("_id").toString();

		carrinhoProduto1 = new CarrinhosProdutosForm(produtoId1, 2);
		carrinhoProduto2 = new CarrinhosProdutosForm(produtoId2, 1);
		produtos.add(carrinhoProduto1);
		produtos.add(carrinhoProduto2);
		carrinho.setProdutos(produtos);
		
		postCarrinhosRequest = new PostCarrinhosRequest();
		postCarrinhosRequest.setCarrinho(carrinho);
		Response postCarrinhoResponse = postCarrinhosRequest.executeRequest();
		int postStatusCode = postCarrinhoResponse.getStatusCode();
		
		cancelarCarrinhosRequest = new CancelarCarrinhosRequest(token);
		cancelarCarrinhosRequest.executeRequest();
		
		deleteProdutosRequest = new DeleteProdutosRequest(produtoId1);
		deleteProdutosRequest.executeRequest();
		deleteProdutosRequest2 = new DeleteProdutosRequest(produtoId2);
		deleteProdutosRequest2.executeRequest();

		assertEquals(postStatusCode, statusCodeEsperado);
	}

}
