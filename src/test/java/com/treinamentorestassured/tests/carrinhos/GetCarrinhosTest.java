package com.treinamentorestassured.tests.carrinhos;

import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.requests.carrinhos.GetCarrinhosRequest;
import com.treinamentorestassured.steps.carrinhos.ListarCarrinhosSteps;

import io.restassured.response.Response;

public class GetCarrinhosTest extends TestBase{
	
	GetCarrinhosRequest getCarrinhosRequest;

	@DataProvider(name = "queryParameter")
	public String[][] queryQarameterTestData() {
		return new String[][] { { "_id", "qbMqntef4iTOwWfg" }, { "precoTotal", "6180" },
				{ "quantidadeTotal", "3" }, { "idUsuario", "oUb7aGkMtSEPf6BZ" }};
	}
	
	@Test
	public void QuandoBuscarDeveRetornarListaDeCarrinhos () {
		
		int statusCodeEsperado = HttpStatus.SC_OK;
		getCarrinhosRequest = new GetCarrinhosRequest();
		Response getCarrinhosresponse = getCarrinhosRequest.executeRequest();
		assertEquals(getCarrinhosresponse.getStatusCode(),statusCodeEsperado);
	}
	
	@Test(dataProvider = "queryParameter")
	public void DadoDadosValidosDeveBuscarCarrinhosComSucesso
	(String parameterName, String parameterValues) {
		
		int statusCodeEsperado = HttpStatus.SC_OK;
		
		getCarrinhosRequest = new GetCarrinhosRequest(parameterName, parameterValues);
		Response getCarrinhosResponse = getCarrinhosRequest.executeRequest();
		int statusCode = getCarrinhosResponse.getStatusCode();
		String id = getCarrinhosResponse.body().jsonPath().get("carrinhos[0]._id").toString();
		String precoTotal = getCarrinhosResponse.body().jsonPath().get("carrinhos[0].precoTotal").toString();
		String quantidadeTotal = getCarrinhosResponse.body().jsonPath().get("carrinhos[0].quantidadeTotal").toString();
		String idUsuario = getCarrinhosResponse.body().jsonPath().get("carrinhos[0].idUsuario").toString();
		
		assertEquals(statusCode, statusCodeEsperado);
		assertEquals(id, "qbMqntef4iTOwWfg");
		assertEquals(precoTotal, "6180");
		assertEquals(quantidadeTotal, "3");
		assertEquals(idUsuario, "oUb7aGkMtSEPf6BZ");
	}

}
