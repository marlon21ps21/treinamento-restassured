package com.treinamentorestassured.tests;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.treinamentorestassured.dbsteps.dataBaseDBSteps;
import com.treinamentorestassured.utils.GeneralUtils;

public class bdTests {

	public void testeDeBanco() {
		dataBaseDBSteps.createDataBase();
		dataBaseDBSteps.createTableProdutos();
		dataBaseDBSteps.dropDataBase();
	}

	@DataProvider(name = "emailsInvalidosCSV")
	public String[] createEmailTestDataCSV() throws IOException {
		String pathCsv = "invalidEmails.csv";
		String[] emails = GeneralUtils.getVetorCsv(pathCsv);
		return emails;
	}

	//@Test(dataProvider = "emailsInvalidosCSV")
	public void DadoEmailInvalidoDeveValidarFormatoCSV(String email) throws IOException {
		System.out.println(email);
	}

}
