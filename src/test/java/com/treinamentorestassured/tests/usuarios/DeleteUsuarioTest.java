package com.treinamentorestassured.tests.usuarios;

import static org.testng.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.UsuariosForm;
import com.treinamentorestassured.requests.usuarios.DeleteUsuarioRequest;
import com.treinamentorestassured.requests.usuarios.PostUsuarioRequest;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class DeleteUsuarioTest extends TestBase {

	PostUsuarioRequest postUsuarioRequest;
	DeleteUsuarioRequest deleteUsuraioRequest;

	@Test
	public void DadoUsuarioCadastradoQuandoTentaRemoverEntaoDeveExibirMensagemDeSucesso() {

		UsuariosForm usuario = GeneralUtils.randomUsuario();
		String valorEsperado = "Registro excluído com sucesso";
		int statusCodeEsperado = HttpStatus.SC_OK;

		postUsuarioRequest = new PostUsuarioRequest();
		postUsuarioRequest.setUsuario(usuario);
		Response postUsuarioResponse = postUsuarioRequest.executeRequest();
		String usuarioId = postUsuarioResponse.body().jsonPath().get("_id").toString();
		
		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		Response deleteUsuarioResponse = deleteUsuraioRequest.executeRequest();
		String message = deleteUsuarioResponse.body().jsonPath().get("message").toString();

		assertEquals(deleteUsuarioResponse.getStatusCode(),statusCodeEsperado);
		assertEquals(message, valorEsperado);
	}

	@Test
	public void DadoUsuarioQueNaoExisteQuandoRemoverDeveMostrarMensagemDeNenhumRegistroExcluido() {

		String valorEsperado = "Nenhum registro excluído";
		int statusCodeEsperado = HttpStatus.SC_OK;
		String usuarioId = "AAAAAAAAAAA";

		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		Response deleteUsuarioResponse = deleteUsuraioRequest.executeRequest();
		String message = deleteUsuarioResponse.body().jsonPath().get("message").toString();

		assertEquals(deleteUsuarioResponse.getStatusCode(),statusCodeEsperado);
		assertEquals(message, valorEsperado);
	}
}
