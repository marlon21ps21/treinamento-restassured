package com.treinamentorestassured.tests.usuarios;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.UsuariosForm;
import com.treinamentorestassured.requests.usuarios.DeleteUsuarioRequest;
import com.treinamentorestassured.requests.usuarios.GetUsuarioRequest;
import com.treinamentorestassured.requests.usuarios.PostUsuarioRequest;
import com.treinamentorestassured.steps.usuarios.CadastrarUsuarioSteps;
import com.treinamentorestassured.steps.usuarios.ExcluirUsuarioSteps;
import com.treinamentorestassured.steps.usuarios.ListarUsuarioSteps;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class PostUsuarioTest extends TestBase {

	GetUsuarioRequest getUsuarioRequest;
	PostUsuarioRequest postUsuarioRequest;
	PostUsuarioRequest postUsuarioRequest2;
	DeleteUsuarioRequest deleteUsuraioRequest;
	
	@Test
	public void DadoFormularioDeUsuarioValidoDeveCadastrar() {

		UsuariosForm usuario = GeneralUtils.randomUsuario();
		String valorEsperado = "Cadastro realizado com sucesso";
		int statusCodeCreated = HttpStatus.SC_CREATED;
		int statusCodeOk = HttpStatus.SC_OK;

		postUsuarioRequest = new PostUsuarioRequest();
		postUsuarioRequest.setUsuario(usuario);
		Response postUsuarioResponse = postUsuarioRequest.executeRequest();
		String usuarioId = postUsuarioResponse.body().jsonPath().get("_id").toString();
		String message = postUsuarioResponse.body().jsonPath().get("message").toString();
		
		getUsuarioRequest = new GetUsuarioRequest("_id", usuarioId);
		Response getUsuarioResponse = getUsuarioRequest.executeRequest();
		String nome = getUsuarioResponse.body().jsonPath().get("usuarios[0].nome").toString();
		String email = getUsuarioResponse.body().jsonPath().get("usuarios[0].email").toString();
		String password = getUsuarioResponse.body().jsonPath().get("usuarios[0].password").toString();
		String administrador = getUsuarioResponse.body().jsonPath().get("usuarios[0].administrador").toString();
		
		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		deleteUsuraioRequest.executeRequest();

		assertEquals(postUsuarioResponse.getStatusCode(),statusCodeCreated);
		assertEquals(message, valorEsperado);
		
		assertEquals(getUsuarioResponse.statusCode(), statusCodeOk);
		assertEquals(nome, usuario.getNome());
		assertEquals(email, usuario.getEmail());
		assertEquals(password, usuario.getPassword());
		assertEquals(administrador, usuario.getAdministrador());

	}
	
	@Test
	public void DadoOMesmoUsuarioNaoDeveCadastrar() {

		UsuariosForm usuario = GeneralUtils.randomUsuario();
		String valorEsperado = "Este email já está sendo usado";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
		int statusCodeOk = HttpStatus.SC_OK;

		postUsuarioRequest = new PostUsuarioRequest();
		postUsuarioRequest.setUsuario(usuario);
		Response postUsuarioResponse = postUsuarioRequest.executeRequest();
		String usuarioId = postUsuarioResponse.body().jsonPath().get("_id").toString();
		
		getUsuarioRequest = new GetUsuarioRequest("_id", usuarioId);
		Response getUsuarioResponse = getUsuarioRequest.executeRequest();
		
		postUsuarioRequest2 = new PostUsuarioRequest();
		postUsuarioRequest2.setUsuario(usuario);
		Response postUsuarioResponse2 = postUsuarioRequest.executeRequest();
		String message = postUsuarioResponse2.body().jsonPath().get("message").toString();
		
		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		deleteUsuraioRequest.executeRequest();

		assertEquals(postUsuarioResponse2.getStatusCode(),statusCodeEsperado);
		assertEquals(message, valorEsperado);
		assertEquals(getUsuarioResponse.getStatusCode(),statusCodeOk);

	}

}
