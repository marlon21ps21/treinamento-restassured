package com.treinamentorestassured.tests.usuarios;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.UsuariosForm;
import com.treinamentorestassured.requests.usuarios.DeleteUsuarioRequest;
import com.treinamentorestassured.requests.usuarios.PostUsuarioRequest;
import com.treinamentorestassured.requests.usuarios.PutUsuarioRequest;
import com.treinamentorestassured.steps.usuarios.AlterarUsuarioSteps;
import com.treinamentorestassured.steps.usuarios.CadastrarUsuarioSteps;
import com.treinamentorestassured.steps.usuarios.ExcluirUsuarioSteps;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class PutUsuarioTest extends TestBase {

	PostUsuarioRequest postUsuarioRequest;
	DeleteUsuarioRequest deleteUsuraioRequest;
	PutUsuarioRequest putUsuarioRequest;
	PostUsuarioRequest postUsuarioRequest1;
	PostUsuarioRequest postUsuarioRequest2;
	DeleteUsuarioRequest deleteUsuraioRequest1;
	DeleteUsuarioRequest deleteUsuraioRequest2;

	@Test
	public void DadoUsuarioValidoQuandoAlterarNomeEntaoDeveMostrarDadoAlterado() {

		UsuariosForm usuario1 = GeneralUtils.randomUsuario();
		UsuariosForm usuario2 = GeneralUtils.randomUsuario();
		String valorEsperado = "Registro alterado com sucesso";
		int statusCodeEsperado = HttpStatus.SC_OK;
		String campo = "message";

		postUsuarioRequest = new PostUsuarioRequest();
		postUsuarioRequest.setUsuario(usuario1);
		Response postUsuarioResponse = postUsuarioRequest.executeRequest();
		String usuarioId = postUsuarioResponse.body().jsonPath().get("_id").toString();
		
		putUsuarioRequest = new PutUsuarioRequest(usuarioId);
		putUsuarioRequest.setUsuario(usuario2);
		Response putUsuarioResponse = putUsuarioRequest.executeRequest();
		String message = putUsuarioResponse.body().jsonPath().get("message").toString();
		int putStatusCode = putUsuarioResponse.getStatusCode();
		
		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		deleteUsuraioRequest.executeRequest();
		
		assertEquals(putStatusCode,statusCodeEsperado);
		assertEquals(message, valorEsperado);

	}
	
	@Test
	public void DadoUsuarioQueNaoExisteQuandoAlterarNomeEntaoDeveCadastrarUsuario() {

		UsuariosForm usuario = GeneralUtils.randomUsuario();
		String usuarioId = "AAAAAAAAAAAAA";
		String valorEsperado = "Cadastro realizado com sucesso";
		int statusCodeEsperado = HttpStatus.SC_CREATED;

		putUsuarioRequest = new PutUsuarioRequest(usuarioId);
		putUsuarioRequest.setUsuario(usuario);
		Response putUsuarioResponse = putUsuarioRequest.executeRequest();
		String message = putUsuarioResponse.body().jsonPath().get("message").toString();
		int putStatusCode = putUsuarioResponse.getStatusCode();
		
		deleteUsuraioRequest = new DeleteUsuarioRequest(usuarioId);
		deleteUsuraioRequest.executeRequest();
		
		assertEquals(putStatusCode,statusCodeEsperado);
		assertEquals(message, valorEsperado);

	}
	
	@Test
	public void DadoUmUsuarioTentarAlterarEmailParaUmQueJaExiste() {

		UsuariosForm usuario1 = GeneralUtils.randomUsuario();
		UsuariosForm usuario2 = GeneralUtils.randomUsuario();
		String valorEsperado = "Este email já está sendo usado";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
		String campo = "message";

		postUsuarioRequest1 = new PostUsuarioRequest();
		postUsuarioRequest1.setUsuario(usuario1);
		Response postUsuarioResponse1 = postUsuarioRequest1.executeRequest();
		String usuarioId1 = postUsuarioResponse1.body().jsonPath().get("_id").toString();
		
		postUsuarioRequest2 = new PostUsuarioRequest();
		postUsuarioRequest2.setUsuario(usuario2);
		Response postUsuarioResponse2 = postUsuarioRequest2.executeRequest();
		String usuarioId2 = postUsuarioResponse2.body().jsonPath().get("_id").toString();
		
		putUsuarioRequest = new PutUsuarioRequest(usuarioId2);
		putUsuarioRequest.setUsuario(usuario1);
		Response putUsuarioResponse = putUsuarioRequest.executeRequest();
		String message = putUsuarioResponse.body().jsonPath().get("message").toString();
		int putStatusCode = putUsuarioResponse.getStatusCode();
		
		deleteUsuraioRequest1 = new DeleteUsuarioRequest(usuarioId1);
		deleteUsuraioRequest1.executeRequest();
		deleteUsuraioRequest2 = new DeleteUsuarioRequest(usuarioId2);
		deleteUsuraioRequest2.executeRequest();
		
		assertEquals(putStatusCode, statusCodeEsperado);
		assertEquals(message, valorEsperado);

	}

}
