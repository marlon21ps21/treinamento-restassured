package com.treinamentorestassured.tests.usuarios;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.requests.usuarios.GetUsuarioRequest;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class GetUsuarioTest extends TestBase {

	GetUsuarioRequest getUsuarioRequest;
	
	@DataProvider(name = "queryParameter")
	public String[][] queryQarameterTestData() {
		return new String[][] { { "_id", "0uxuPY0cbmQhpEz1" }, { "nome", "Fulano da Silva" },
				{ "email", "fulano@qa.com" }, { "password", "teste" }, { "administrador", "true" }};
	}

	@Test
	public void DeveBuscarListaDeUsuariosSemFiltro() {

		int statusCodeEsperado = HttpStatus.SC_OK;
		String nome = "Fulano da Silva";
		String email = "fulano@qa.com";
		String password = "teste";
		String administrador = "true";
		String usuarioId = "0uxuPY0cbmQhpEz1";

		getUsuarioRequest = new GetUsuarioRequest();
		Response response = getUsuarioRequest.executeRequest();

		assertEquals(response.getStatusCode(),statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("usuarios[0].nome").toString(), nome);
		assertEquals(response.body().jsonPath().get("usuarios[0].email").toString(), email);
		assertEquals(response.body().jsonPath().get("usuarios[0].password").toString(), password);
		assertEquals(response.body().jsonPath().get("usuarios[0].administrador").toString(), administrador);
		assertEquals(response.body().jsonPath().get("usuarios[0]._id").toString(), usuarioId);
		assertTrue(GeneralUtils.validateEmailFormat(response.body().jsonPath().get("usuarios[0].email").toString()));
	
	}

	@Test(dataProvider = "queryParameter")
	public void DadoQueryParameterValidosDeveBuscarUsuariosEntaoDeveListarUsuarios(String parameterName,
			String parameterValues) {

		int statusCodeEsperado = HttpStatus.SC_OK;
		String nome = "Fulano da Silva";
		String email = "fulano@qa.com";
		String password = "teste";
		String administrador = "true";

		getUsuarioRequest = new GetUsuarioRequest(parameterName, parameterValues);
		Response response = getUsuarioRequest.executeRequest();

		assertEquals(response.getStatusCode(),statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("usuarios[0].nome").toString(), nome);
		assertEquals(response.body().jsonPath().get("usuarios[0].email").toString(), email);
		assertEquals(response.body().jsonPath().get("usuarios[0].password").toString(), password);
		assertEquals(response.body().jsonPath().get("usuarios[0].administrador").toString(), administrador);
		assertTrue(GeneralUtils.validateEmailFormat(response.body().jsonPath().get("usuarios[0].email").toString()));
	}
	
	@Test
	public void DeveBuscarUsuarioPeloIdPath() {

		SoftAssert softAssert = new SoftAssert();
		int statusCodeEsperado = HttpStatus.SC_OK;
		String nome = "Fulano da Silva";
		String email = "fulano@qa.com";
		String password = "teste";
		String administrador = "true";
		String usuarioId = "0uxuPY0cbmQhpEz1";

		getUsuarioRequest = new GetUsuarioRequest(usuarioId);
		Response response = getUsuarioRequest.executeRequest();

		softAssert.assertEquals(response.getStatusCode(),statusCodeEsperado);
		softAssert.assertEquals(response.body().jsonPath().get("_id").toString(), usuarioId);
		softAssert.assertEquals(response.body().jsonPath().get("email").toString(), email);
		softAssert.assertEquals(response.body().jsonPath().get("password").toString(), password);
		softAssert.assertEquals(response.body().jsonPath().get("administrador").toString(), administrador);
		softAssert.assertEquals(response.body().jsonPath().get("nome").toString(), nome);
		softAssert.assertAll();
	
	}
}
