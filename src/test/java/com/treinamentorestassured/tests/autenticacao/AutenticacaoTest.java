package com.treinamentorestassured.tests.autenticacao;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.treinamentorestassured.bases.TestBase;
import com.treinamentorestassured.forms.AutenticacaoForm;
import com.treinamentorestassured.requests.AutenticacaoRequest;
import com.treinamentorestassured.utils.GeneralUtils;

import io.restassured.response.Response;

public class AutenticacaoTest extends TestBase {

	@DataProvider(name = "emailsInvalidos")
	public String[] createEmailTestData() {
		return new String[] { "fulano", "fulano@qa", "@qa.com", "@.com", "@" };
	}

	@DataProvider(name = "emailsInvalidosCSV")
	public String[] createEmailTestDataCSV() throws IOException {
		String pathCsv = "invalidEmails.csv";
		String[] emails = GeneralUtils.getVetorCsv(pathCsv);
		return emails;
	}

	@Test
	public void DadoUsuarioValidoDeveAutenticarComSucesso() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("teste");
		String message = "Login realizado com sucesso";
		int statusCodeEsperado = HttpStatus.SC_OK;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("message").toString(), message);
	}

	@Test
	public void DadoEmailDoUsuarioInValidoNaoDeveAutenticar() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("ffulano@qa.com");
		autenticacao.setPassword("teste");
		String message = "Email e/ou senha inválidos";
		int statusCodeEsperado = HttpStatus.SC_UNAUTHORIZED;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("message").toString(), message);
	}

	@Test
	public void DadoSenhaDoUsuarioInValidoNaoDeveAutenticar() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("testes");
		String message = "Email e/ou senha inválidos";
		int statusCodeEsperado = HttpStatus.SC_UNAUTHORIZED;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("message").toString(), message);
	}

	@Test
	public void DadoSenhaEEmailDoUsuarioInValidosNaoDeveAutenticar() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("fffulano@qa.com");
		autenticacao.setPassword("tttestes");
		String message = "Email e/ou senha inválidos";
		int statusCodeEsperado = HttpStatus.SC_UNAUTHORIZED;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("message").toString(), message);
	}

	@Test
	public void DeveVailidarCamposObrigatorios() {

		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		String emailMessage = "email é obrigatório";
		String passwordMessage = "password é obrigatório";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(null);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("email").toString(), emailMessage);
		assertEquals(response.body().jsonPath().get("password").toString(), passwordMessage);
	}

	@Test
	public void DadoEmailEmBrancoDeveValidarCampo() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("");
		autenticacao.setPassword("teste");
		String message = "email não pode ficar em branco";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("email").toString(), message);
	}

	@Test
	public void DadoPasswordEmBrancoDeveValidarCampo() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("fulano@qa.com");
		autenticacao.setPassword("");
		String message = "password não pode ficar em branco";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("password").toString(), message);
	}

	@Test
	public void DadoEmailEPasswordEmBrancoDeveValidarCampos() {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail("");
		autenticacao.setPassword("");
		String emailMessage = "email não pode ficar em branco";
		String passwordMessage = "password não pode ficar em branco";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("email").toString(), emailMessage);
		assertEquals(response.body().jsonPath().get("password").toString(), passwordMessage);
	}

	@Test(dataProvider = "emailsInvalidos")
	public void DadoEmailInvalidoDeveValidarFormato(String email) {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail(email);
		autenticacao.setPassword("teste");
		String emailMessage = "email deve ser um email válido";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("email").toString(), emailMessage);
	}

	@Test(dataProvider = "emailsInvalidosCSV")
	public void DadoEmailInvalidoDeveValidarFormatoCSV(String email) {

		AutenticacaoForm autenticacao = new AutenticacaoForm();
		AutenticacaoRequest autenticacaoRequest = new AutenticacaoRequest();
		autenticacao.setEmail(email);
		autenticacao.setPassword("teste");
		String emailMessage = "email deve ser um email válido";
		int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

		autenticacaoRequest.setAutenticacaoForm(autenticacao);
		Response response = autenticacaoRequest.executeRequest();

		assertEquals(response.getStatusCode(), statusCodeEsperado);
		assertEquals(response.body().jsonPath().get("email").toString(), emailMessage);
	}
}
