package com.treinamentorestassured;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class GlobalParameters {

	public static String ENVIROMENT;
	public static String URL_DEFAULT;
	public static String REPORT_NAME;
	public static String REPORT_PATH;
	public static String TOKEN;
	public static String AUTHENTICATOR_USER;
	public static String AUTHENTICATOR_PASSWORD;
	public static String DB_URL;
    public static String DB_NAME;
    public static String DB_USER;
    public static String DB_PASSWORD;  

	private Properties properties;

	public GlobalParameters() {

		properties = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("globalParameters.properties");
			properties.load(input);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		REPORT_NAME = properties.getProperty("report.name");
		REPORT_PATH = properties.getProperty("report.path");
		ENVIROMENT = properties.getProperty("enviroment");

		if (ENVIROMENT.equals("dev")) {
			URL_DEFAULT = properties.getProperty("dev.url");
			DB_URL = properties.getProperty("dev.db.url");
            DB_NAME = properties.getProperty("dev.db.name");
            DB_USER = properties.getProperty("dev.db.user");
            DB_PASSWORD = properties.getProperty("dev.db.password");
		}

		if (ENVIROMENT.equals("hml")) {
			URL_DEFAULT = properties.getProperty("hml.url");
			DB_URL = properties.getProperty("hml.db.url");
            DB_NAME = properties.getProperty("hml.db.name");
            DB_USER = properties.getProperty("hml.db.user");
            DB_PASSWORD = properties.getProperty("hml.db.password");
		}

	}

	public static void setToken(String token) {
		TOKEN = token;
	}

}
