package com.treinamentorestassured.bases;

import java.util.HashMap;
import java.util.Map;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.enums.AuthenticationType;
import com.treinamentorestassured.forms.UsuariosForm;
import com.treinamentorestassured.utils.ExtentReportsUtils;
import com.treinamentorestassured.utils.RestAssuredUtils;

import io.restassured.http.Method;
import io.restassured.response.Response;

public abstract class RequestRestBase {

	protected String url = GlobalParameters.URL_DEFAULT;
	protected String requestService = null;
	protected Method method = null;
	protected String jsonBody = null;
	protected Object form = null;
	protected Map<String, String> headers = new HashMap<String, String>();
	protected Map<String, String> queryParameters = new HashMap<String, String>();
	protected String token;
	protected AuthenticationType authenticationType = AuthenticationType.NONE;
	protected String authenticatorUser = GlobalParameters.AUTHENTICATOR_USER;
	protected String authenticatorPassword = GlobalParameters.AUTHENTICATOR_PASSWORD;

	public RequestRestBase() {
		headers.put("content-type", "application/json");
	}
	
	public Response executeRequest() {
		Response response = RestAssuredUtils.executeRestRequest
				(url, requestService, method, headers, queryParameters, jsonBody, form, authenticationType);
		ExtentReportsUtils.addRestTestInfo
		(url, requestService, method.toString(), headers, jsonBody,
				authenticatorUser, authenticatorPassword, response);

		return response;
	}
}
