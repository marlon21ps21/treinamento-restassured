package com.treinamentorestassured.forms;

public class CarrinhosProdutosForm {

	private String idProduto;
	private int quantidade;
	
	@Deprecated
	public CarrinhosProdutosForm() {	
	}
	
	public CarrinhosProdutosForm(String idProduto, int quantidade) {
		this.idProduto = idProduto;
		this.quantidade = quantidade;
	}

	public String getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(String idProduto) {
		this.idProduto = idProduto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
}
