package com.treinamentorestassured.forms;

import java.util.ArrayList;
import java.util.List;

public class CarrinhosForm {
	
	List<CarrinhosProdutosForm> produtos = new ArrayList<>();

	public List<CarrinhosProdutosForm> getProdutos() {
		return produtos;
	}
	
	public void setProdutos(List<CarrinhosProdutosForm> produtos) {
		this.produtos = produtos;
	}
	
}
