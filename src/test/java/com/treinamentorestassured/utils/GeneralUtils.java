package com.treinamentorestassured.utils;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.testng.ITestResult;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.treinamentorestassured.forms.UsuariosForm;

public class GeneralUtils {

	public static String readFileToAString(String path) {
		byte[] encoded = new byte[0];
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(encoded, StandardCharsets.ISO_8859_1);
	}

	public static String getNowDate(String mask) {
		DateFormat dateFormat = new SimpleDateFormat(mask);
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String getAllStackTrace(ITestResult result) {
		String allStackTrace = "";

		for (StackTraceElement stackTrace : result.getThrowable().getStackTrace()) {
			allStackTrace = allStackTrace + "<br>" + stackTrace.toString();
		}

		return allStackTrace;
	}

	public static String getMethodNameByLevel(int level) {
		StackTraceElement[] stackTrace = new Throwable().getStackTrace();

		return stackTrace[level].getMethodName();
	}

	public static String formatJson(final String json) {
		final int indent_width = 4;
		final char[] chars = json.toCharArray();
		final String newline = System.lineSeparator();

		String ret = "";
		boolean begin_quotes = false;

		for (int i = 0, indent = 0; i < chars.length; i++) {
			char c = chars[i];

			if (c == '\"') {
				ret += c;
				begin_quotes = !begin_quotes;
				continue;
			}

			if (!begin_quotes) {
				switch (c) {
				case '{':
				case '[':
					ret += c + newline + String.format("%" + (indent += indent_width) + "s", "");
					continue;
				case '}':
				case ']':
					ret += newline + ((indent -= indent_width) > 0 ? String.format("%" + indent + "s", "") : "") + c;
					continue;
				case ':':
					ret += c + " ";
					continue;
				case ',':
					ret += c + newline + (indent > 0 ? String.format("%" + indent + "s", "") : "");
					continue;
				default:
					if (Character.isWhitespace(c))
						continue;
				}
			}

			ret += c + (c == '\\' ? "" + chars[++i] : "");
		}

		return ret;
	}

	public static boolean validateEmailFormat(String email) {

		return email.matches("^[A-Za-z0-9_.-]+@[A-Za-z0-9_.-]+\\.[a-z]+$");
	}

	public static String randomPassword(int len) {

		String chave = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		SecureRandom random = new SecureRandom();
		StringBuilder password = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			password.append(chave.charAt(random.nextInt(chave.length())));
		}
		return password.toString();
	}

	public static String randomUserName() {

		JSONArray jsonArray = new JSONArray(GeneralUtils.readFileToAString("src/test/resources/listanomes.json"));
		return jsonArray.getString(getRandomNumber(jsonArray.length()));
	}

	public static int getRandomNumber(int max) {
		Random random = new Random();
		return random.nextInt(max);
	}

	public static String randomUserEmail(String name) {
		return name.replace(" ", "") + "@qa.com";
	}

	public static String randomAdministrator() {

		return (getRandomNumber(2) == 1) ? "false" : "true";
	}

	public static UsuariosForm randomUsuario() {
		UsuariosForm usuario = new UsuariosForm();
		usuario.setNome(randomUserName());
		usuario.setEmail(randomUserEmail(usuario.getNome()));
		usuario.setPassword(randomPassword(10));
		usuario.setAdministrador(randomAdministrator());
		return usuario;
	}

	public static List<String[]> readFileCsv(String pathCsv) {
		Reader reader;
		List<String[]> dados = new ArrayList<String[]>();
		try {
			reader = Files.newBufferedReader(Paths.get(pathCsv));
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
			dados = csvReader.readAll();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return dados;
	}

	public static String[] getVetorCsv(String pathCsv) {
		List<String[]> dados = readFileCsv(pathCsv);
		List<String> dadosList = new ArrayList<>();
		for (int i = 0; i < dados.size(); i++) {
			dadosList.add(dados.get(i)[0]);
		}
		String[] dado = new String[dadosList.size()];
		dadosList.toArray(dado);
		return dado;
	}

}
