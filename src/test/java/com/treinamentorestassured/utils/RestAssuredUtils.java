package com.treinamentorestassured.utils;

import java.net.URI;
import java.util.Map;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.enums.AuthenticationType;
import com.treinamentorestassured.forms.UsuariosForm;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredUtils {

	public static Response executeRestRequest(String url, String requestService, Method method,
			Map<String, String> headers, Map<String, String> queryParameters, String jsonBody, Object form,
			AuthenticationType authenticationType) {

		RequestSpecification requestSpecification = RestAssured.given();

		if (authenticationType == AuthenticationType.OAUT2) {
			requestSpecification.auth().oauth2(GlobalParameters.TOKEN.substring(7));
		}

		for (Map.Entry<String, String> header : headers.entrySet()) {
			requestSpecification.headers(header.getKey(), header.getValue());
		}

		for (Map.Entry<String, String> parameter : queryParameters.entrySet()) {
			requestSpecification.queryParams(parameter.getKey(), parameter.getValue());
		}

		if (jsonBody != null) {
			requestSpecification.body(jsonBody);
		}
		if (form != null) {
			requestSpecification.body(form);
		}

		return requestSpecification.request(method, URI.create(url + requestService));
	}

}
