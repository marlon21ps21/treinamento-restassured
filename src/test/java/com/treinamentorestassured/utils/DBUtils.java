package com.treinamentorestassured.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.treinamentorestassured.GlobalParameters;
import com.treinamentorestassured.forms.ProdutoForm;

public class DBUtils {

	private static String getStringConnection() {
		return "jdbc:mysql://" + GlobalParameters.DB_URL + "/" + GlobalParameters.DB_NAME
				+ "?useTimezone=true&serverTimezone=UTC";
	}

	public static void executeQuery(String query) {
		Connection connection = null;

		try {
			Statement stmt = null;
			connection = DriverManager.getConnection(getStringConnection(), GlobalParameters.DB_USER,
					GlobalParameters.DB_PASSWORD);

			stmt = connection.createStatement();
			stmt.executeQuery(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<ProdutoForm> getProdutosQuery(String query) {

		List<ProdutoForm> produtos = new ArrayList<ProdutoForm>();
		Connection connection = null;

		try {

			Statement stmt = null;
			connection = DriverManager.getConnection(getStringConnection(), GlobalParameters.DB_USER,
					GlobalParameters.DB_PASSWORD);

			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				ProdutoForm produto = new ProdutoForm();
				produto.setNome(rs.getString("nome"));
				produto.setPreco(rs.getString("preco"));
				produto.setDescricao(rs.getString("descricao"));
				produto.setQuantidade(rs.getString("quantidade"));
				produtos.add(produto);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return produtos;

	}

	public static void executeUpdateQuery(String query) {

		Connection connection = null;

		try {
			Statement stmt = null;
			connection = DriverManager.getConnection(getStringConnection(), GlobalParameters.DB_USER,
					GlobalParameters.DB_PASSWORD);

			stmt = connection.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public static void executeCreateDataBaseQuery(String query) {

		Connection connection = null;

		try {
			Statement stmt = null;
			connection = DriverManager.getConnection("jdbc:mysql://" + GlobalParameters.DB_URL + "?useTimezone=true&serverTimezone=UTC",
					GlobalParameters.DB_USER,
					GlobalParameters.DB_PASSWORD);

			stmt = connection.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
