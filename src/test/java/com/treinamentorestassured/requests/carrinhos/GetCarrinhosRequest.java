package com.treinamentorestassured.requests.carrinhos;

import com.treinamentorestassured.bases.RequestRestBase;

import io.restassured.http.Method;

public class GetCarrinhosRequest extends RequestRestBase {

	public GetCarrinhosRequest() {

		requestService = "/carrinhos";
		method = Method.GET;
	}

	public GetCarrinhosRequest(String parameterName, String parameterValues) {
		requestService = "/Carrinhos";
		method = Method.GET;
		queryParameters.put(parameterName, parameterValues);

	}

}
