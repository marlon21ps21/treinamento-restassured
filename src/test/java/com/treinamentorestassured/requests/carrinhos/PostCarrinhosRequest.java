package com.treinamentorestassured.requests.carrinhos;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.enums.AuthenticationType;
import com.treinamentorestassured.forms.CarrinhosForm;

import io.restassured.http.Method;

public class PostCarrinhosRequest extends RequestRestBase{
	
	public PostCarrinhosRequest() {

		requestService = "/carrinhos";
		method = Method.POST;
		authenticationType = AuthenticationType.OAUT2;
	}

	public void setCarrinho(CarrinhosForm carrinho) {	
		form = carrinho;	
	}

}
