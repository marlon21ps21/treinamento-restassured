package com.treinamentorestassured.requests.carrinhos;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.enums.AuthenticationType;

import io.restassured.http.Method;

public class CancelarCarrinhosRequest extends RequestRestBase {
	
	public CancelarCarrinhosRequest(String token) {

		requestService = "/carrinhos/cancelar-compra";
		method = Method.DELETE;
		authenticationType = AuthenticationType.OAUT2;
	}
}
