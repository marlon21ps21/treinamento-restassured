package com.treinamentorestassured.requests;


import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.forms.AutenticacaoForm;

import io.restassured.http.Method;

public class AutenticacaoRequest extends RequestRestBase {

	public AutenticacaoRequest() {
		requestService = "/login";
		method = Method.POST;
	}
	
	public void setAutenticacaoForm(AutenticacaoForm autenticacao) {
		form = autenticacao;
	}

}
