package com.treinamentorestassured.requests.usuarios;

import com.treinamentorestassured.bases.RequestRestBase;

import io.restassured.http.Method;

public class DeleteUsuarioRequest extends RequestRestBase {

	public DeleteUsuarioRequest(String usuarioId) {

		requestService = "/usuarios/" + usuarioId;
		method = Method.DELETE;
	}

}
