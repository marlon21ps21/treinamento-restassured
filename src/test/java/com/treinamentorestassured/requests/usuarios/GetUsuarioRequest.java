package com.treinamentorestassured.requests.usuarios;

import com.treinamentorestassured.bases.RequestRestBase;

import io.restassured.http.Method;

public class GetUsuarioRequest extends RequestRestBase {

	public GetUsuarioRequest() {
		requestService = "/usuarios";
		method = Method.GET;
	}

	public GetUsuarioRequest(String parameterName, String parameterValues) {
		requestService = "/usuarios";
		method = Method.GET;	
		queryParameters.put(parameterName, parameterValues);
		
	}
	
	public GetUsuarioRequest(String path) {
		requestService = "/usuarios/" + path;
		method = Method.GET;
		
	}

}
