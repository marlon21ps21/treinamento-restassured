package com.treinamentorestassured.requests.usuarios;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.forms.UsuariosForm;

import io.restassured.http.Method;

public class PutUsuarioRequest extends RequestRestBase {
	
	public PutUsuarioRequest(String usuarioId) {

		requestService = "/usuarios/" + usuarioId;
		method = Method.PUT;
	}

	public void setUsuario(UsuariosForm usuario) {
		form = usuario;
	}


}
