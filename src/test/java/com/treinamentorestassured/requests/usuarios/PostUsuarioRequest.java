package com.treinamentorestassured.requests.usuarios;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.forms.UsuariosForm;

import io.restassured.http.Method;

public class PostUsuarioRequest extends RequestRestBase {

	public PostUsuarioRequest() {

		requestService = "/usuarios";
		method = Method.POST;
	}

	public void setUsuario(UsuariosForm usuario) {
		form = usuario;
	}

}
