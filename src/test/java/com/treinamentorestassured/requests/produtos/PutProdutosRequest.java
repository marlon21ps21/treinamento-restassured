package com.treinamentorestassured.requests.produtos;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.enums.AuthenticationType;
import com.treinamentorestassured.forms.ProdutoForm;

import io.restassured.http.Method;

public class PutProdutosRequest extends RequestRestBase {

	public PutProdutosRequest(String produtoId) {

		requestService = "/produtos/" + produtoId;
		method = Method.PUT;
		authenticationType = AuthenticationType.OAUT2;
	}

	public void setProduto(ProdutoForm produto) {
		form = produto;
	}

}
