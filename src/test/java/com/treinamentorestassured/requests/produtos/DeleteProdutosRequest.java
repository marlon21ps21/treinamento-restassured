package com.treinamentorestassured.requests.produtos;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.enums.AuthenticationType;

import io.restassured.http.Method;

public class DeleteProdutosRequest extends RequestRestBase{
	
	public DeleteProdutosRequest(String usuarioId) {

		requestService = "/produtos/" + usuarioId;
		method = Method.DELETE;
		authenticationType = AuthenticationType.OAUT2;
	}

}
