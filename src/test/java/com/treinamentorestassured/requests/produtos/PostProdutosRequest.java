package com.treinamentorestassured.requests.produtos;

import com.treinamentorestassured.bases.RequestRestBase;
import com.treinamentorestassured.enums.AuthenticationType;
import com.treinamentorestassured.forms.ProdutoForm;

import io.restassured.http.Method;

public class PostProdutosRequest extends RequestRestBase {
	
	public PostProdutosRequest() {

		requestService = "/produtos";
		method = Method.POST;
		authenticationType = AuthenticationType.OAUT2;
	}

	public void setProduto(ProdutoForm produto) {	
		form = produto;	
	}

}
