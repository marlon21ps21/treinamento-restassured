package com.treinamentorestassured.requests.produtos;

import com.treinamentorestassured.bases.RequestRestBase;

import io.restassured.http.Method;

public class GetProdutosRequest extends RequestRestBase{
	
	public GetProdutosRequest() {
		requestService = "/produtos";
		method = Method.GET;
	}
	
	public GetProdutosRequest(String query, String value) {
		requestService = "/produtos";
		method = Method.GET;
		queryParameters.put(query, value);
	}
	
	public GetProdutosRequest(String path) {
		requestService = "/produtos/" + path;
		method = Method.GET;
	}

}
